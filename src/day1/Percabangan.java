package day1;

import java.util.Scanner;

public class Percabangan {
	public static void main(String[] args) {
//		System.out.println("Selamat Pagi");
//		System.out.println("Selamat Datang");
//		
//		boolean mauKeluar = false;
//		
//		if (mauKeluar) {
//			System.out.println("Selamat Jalan");
//		}else {
//			System.out.println("Tidak Jadi Keluar");
//			
//		}
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan Ruangan: ");
		String ruangan = input.next();
		System.out.print("Masukan Nama Siswa: ");
		String nama = input.next();
		
		
		System.out.print("Masukan Nilai Siswa: ");
		int nilai = input.nextInt();
		System.out.print("Masukan Nilai Soft Skil: ");
		int nilaiSoftSkill = input.nextInt();
		
		input.nextLine();
		System.out.print("Masukan Nama Batch: ");
		String batch = input.nextLine();
		String grade = "";
		String kelulusan = "";
		
		
		if (nilai>=90 && nilaiSoftSkill>=3) {
			kelulusan = "LULUS";
			grade = "A";
		}else if(nilai>=85 && nilaiSoftSkill>=3) {
			kelulusan = "LULUS";
			grade = "B+";
		}else if(nilai>=80 && nilaiSoftSkill>=3) {
			kelulusan = "LULUS";
			grade = "B";
		}else if(nilai>=75 && nilaiSoftSkill>=3) {
			kelulusan = "LULUS";
			grade = "C+";
		}else if(nilai>=70 && nilaiSoftSkill>=3) {
			kelulusan = "REMEDIAL";
			grade = "C";
		}
		else {
			kelulusan = "TIDAK LULUS";
			grade = "D";
		}
		
		
		System.out.print(nama+" Dari "+batch+" "+kelulusan+" Dengan Grade "+grade+" Dari Ruangan "+ruangan);
		
	
	}
	
	
}
