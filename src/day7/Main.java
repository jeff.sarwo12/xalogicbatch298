package day7;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Scanner;

public class Main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Pilih Nomor Soal (3-10 : )");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case id not available");
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}
	private static void soal3() {
		System.out.println("Simple Array Sum");
		System.out.println("Inputkan n : ");
		int n = input.nextInt();
		int total = 0 ;
		for (int i = 0; i < n; i++) {
			System.out.println("Input "+(i+1)+" = ");
			int n2 = input.nextInt();
			total = total+n2;
		}
		System.out.println("Total : "+total);
	}
	private static void soal4() {
		System.out.println("Diagonal Difference");
		
		System.out.println("Input matrik : ");
		int n = input.nextInt();
		int pri = 0;
		int sec = 0;
		int[][] arrMat = new int [n][n];
		
		for (int i = 0; i < arrMat.length; i++) {
			for (int j = 0; j < arrMat.length; j++) {
				System.out.print("Input nilai matrik ("+i+","+j+") : ");
				arrMat[i][j]= input.nextInt();
				if (i==j) {
					pri = pri+arrMat[i][j];
				}
				if(i+j==2){
					sec = sec+arrMat[i][j];
				}
			}
		}
		
		int jum = pri-sec;
		jum = Math.abs(jum);
//		System.out.println("Prim : "+pri);
//		System.out.println("Sec : "+sec);
		System.out.println("Jumlah : "+jum);
		
		
	}
	private static void soal5() {
		System.out.println("Plus Minus");
		
		System.out.println("Input n : ");
		int n = input.nextInt();
		int tem = 0; 
		double ples = 0;
		double neg = 0;
		double nol = 0;
		for (int i = 0; i < n; i++) {
			System.out.print("Input nilai "+(i+1)+" : ");
			tem = input.nextInt();
			if (tem > 0) {
				ples++;
			} else if(tem<0) {
				neg++;
			}else {
				nol++;
			}
		}
		DecimalFormat des = new DecimalFormat("0.000000");
		ples = ples/n;
		neg = neg/n;
		nol = nol/n;
		
		System.out.println(des.format(ples));
		System.out.println(des.format(neg));
		System.out.println(des.format(nol));
		
		
	}
	private static void soal6() {
		System.out.println("Staircase");
		System.out.println("Input Matrik : ");
		int n = input.nextInt();
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i+j>=(n-1)) {
					System.out.print("#");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
	private static void soal7() {
		System.out.println("Mini-Max Sum");
		System.out.println("Input array : ");
		String n = input.nextLine();
		String[] arrNilai = n.split(" ");
		int [] arrJmlah = new int[arrNilai.length];
		
		for (int i = 0; i < arrNilai.length; i++) {
			arrJmlah[i]=0;
			for (int j = 0; j < arrNilai.length; j++) {
				if (Integer.parseInt(arrNilai[i])!=Integer.parseInt(arrNilai[j])) {
					int temp = Integer.parseInt(arrNilai[j]);
					arrJmlah[i] = arrJmlah[i]+temp;  
				}
			}
		}
		int max = arrJmlah[0];
		int min = arrJmlah[0];
		for (int i = 0; i < arrJmlah.length; i++) {
			if (arrJmlah[i]>max) {
				max = arrJmlah[i];
			} else if(arrJmlah[i]<min) {
				min = arrJmlah[i];
			}
//			System.out.print(arrJmlah[i]+" ");
		}
		
		System.out.println("Nilai Maks : "+max);
		System.out.println("Nilai Min : "+min);
	}

	private static void soal8() {
		System.out.println("Birthday Cake Candles");
		
		System.out.println("Input jumlah inputan n : ");
		int n = input.nextInt();
		int[] arrNilai = new int[n];
		for (int i = 0; i < n; i++) {
			System.out.print("Masukan nilai : ");
			arrNilai[i]=input.nextInt();
		}
		int height = 0;
//		int height2 =1;
		int jum = 0;
		for (int i = 0; i < arrNilai.length; i++) {
			if (arrNilai[i] > height) {
				jum = 0;
				height = arrNilai[i];
				for (int j = 0; j < arrNilai.length; j++) {
					if (arrNilai[i]==arrNilai[j]) {
						jum++;
					} 
				}
			} 
			
		} 
		
		System.out.println("Nilai Tertinggi : "+jum);
	}
	private static void soal9() {
		System.out.println("A Very Big SumPage");
		System.out.println("Inputkan jumlah inputan : ");
		int n = input.nextInt();
		long jum = 0;
		for (int i = 0; i < n; i++) {
			System.out.println("Input nilai "+(i+1)+" : ");
			long nil = input.nextInt();
			jum += nil;
		}
		
		System.out.println(jum);
		
	}
	private static void soal10() {
		System.out.println(" Compare the Triplets");
		System.out.println("Inputkan a : ");
		String nilaiA = input.nextLine();
		String [] arrNilaiA =  nilaiA.split(" ");
		System.out.println("Inputkan b : ");
		String nilaiB = input.nextLine();
		String [] arrNilaiB =  nilaiB.split(" ");
		int pointA = 0;
		int pointB = 0;
		
		for (int i = 0; i < arrNilaiA.length; i++) {
			int a = Integer.parseInt(arrNilaiA[i]);
			int b = Integer.parseInt(arrNilaiB[i]);
			
			if (a>b) {
				pointA++;
			} else if(b>a) {
				pointB++;
			}
		}
		
		System.out.println(""+pointA+" "+pointB);
		
	}
}
