package day14;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);
	public static void main(String[] args) {
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Pilih Nomor Soal 1-2");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				default:
					System.out.println("Soal Tidak Ditemukan!");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println();
			System.out.println("Lanjut?");
			answer = input.nextLine();
		}

	}
	private static void soal1() throws ParseException {
		System.out.println("Soal Mary");
		System.out.print("x : ");
		int x = input.nextInt();
		System.out.println();
		System.out.print("y : ");
		int y = input.nextInt();
		input.nextLine();
		System.out.println("Input tanggal z (dd MMMM yyyy) :");
		String inputTgl = input.nextLine();
		String tgl = inputTgl.replace(" ", "-"); 
		String[] arrTgl = inputTgl.split(" ");
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy", new java.util.Locale("id"));
		Date tglLibur = dateFormat.parse(tgl);
		int t = Integer.parseInt(arrTgl[0]);
		int thn = Integer.parseInt(arrTgl[2]); 
		int bln = tglLibur.getMonth();
//		System.out.println(t+"-"+bln+"-"+thn);
		
		if (bln==1 || bln==3 || bln == 5 || bln == 7 || bln == 8 || bln == 10 || bln == 12) {
			if (t>31) {
				System.out.println("Tanggal Kelebihan ! Ulang ! ");
				return;
			}
		}else if (bln==4 || bln==6 || bln == 9 || bln == 11 ) {
			if (t>30) {
				System.out.println("Tanggal Kelebihan ! Ulang ! ");
				return;
			}
		}else if(bln == 2 && thn % 4 == 0) {
			if (t>29) {
				System.out.println("Tanggal Kelebihan ! Ulang ! ");
				return;
			}
		}else if(bln == 2 && thn % 4 != 0) {
			if (t>28) {
				System.out.println("Tanggal Kelebihan ! Ulang ! ");
				return;
			}
		}
		
		
		int libur = 0;
		if (x==y || (x+1)%(y+1)==0 || (y+1)%(x+1)==0) {
			if (x>y) {
				libur = x+1;
			}
			else {
				libur = y+1;
			}
		}
		else {
			libur = (x+1)*(y+1);
		}
		long tambah = libur * 24 * 60 * 60 * 1000;
		long hitung = tglLibur.getTime()+tambah;
		
		Date hasil = new Date(hitung);
		String tampil = ""+dateFormat.format(hasil);
		tampil = tampil.replace("-", " ");
		System.out.println(tampil);
	}
	private static void soal2() {
		System.out.println("Masukan teks : ");
		String teks = input.nextLine();
		teks = teks.toLowerCase().replace(" ", "");
		char arrTeks [] = teks.toCharArray();
//		System.out.println(arrTeks.length());
		char temp = 0;
		for (int i = 0; i < arrTeks.length; i++) {
			for (int j = 0; j < arrTeks.length; j++) {
				if (arrTeks[i]<arrTeks[j]) {
					temp = arrTeks[i];
					arrTeks[i] = arrTeks[j];
					arrTeks[j] = temp;
				}
			}
		}
		temp = 0;
		for (int i = 0; i < arrTeks.length; i++) {
			if (temp==arrTeks[i]||i==0) {
				System.out.print(arrTeks[i]);
			}
			else {
				System.out.print("-"+arrTeks[i]);
			}
			temp = arrTeks[i];
		}
		
		
	}

}
