package day5;

import java.util.Iterator;
import java.util.Scanner;
import java.util.Arrays;

public class Main {
	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		String answer = "Y";
		
		while(answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();
				
				switch(number) {
					case 1:
						soal1();
						break;
					case 2:
						soal2();
						break;
					case 3:
						soal3();
						break;
					case 4:
						soal4();
						break;
					case 5:
						soal5();
						break;
					case 6:
						soal6();
						break;
					default:
						System.out.println("Case id not available");
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
		

	}
	
	private static void soal1() {
		System.out.println();
		System.out.println("Uang Adi : ");
		int uang = input.nextInt();
		int max = 0;
		
		input.nextLine();
		System.out.println("Harga Baju : ");
		String stringBaju = input.nextLine();
//		String stringBaju ="35,40,50,20";
		System.out.println("Harga Celana : ");
		String stringCelana = input.nextLine();
//		String stringCelana ="40,30,45,10";
		
		String[] arrBaju = stringBaju.split(",");
		String[] arrCelana = stringCelana.split(",");
		int [] total = new int[arrBaju.length];
		for (int i = 0; i < arrBaju.length; i++) {
			total[i] = Integer.parseInt(arrBaju[i])+Integer.parseInt(arrCelana[i]);
		}

		for (int i = 0; i < total.length; i++) {
			if (total[i]<uang && total[i]>max) {
				max = total[i];
			} 
			
		}
		System.out.println(max);
		System.out.println();
//		System.out.println(arrBaju[2]);
	}
	
	private static void soal2() {
		System.out.println("Di sini soal 2");
		
		System.out.println("Masukan Array : ");
		String teks = input.nextLine();
//		String teks = "5,6,7,0,1";
		System.out.println("Masukan Jumlah Rotasi : ");
		int rot = input.nextInt();
		String[] arrString = teks.split(",");
		String temp = "";
		
		for (int i = 0; i < rot; i++) {
			String hasil = "";
			for(int j = 0; j < arrString.length; j++) {
				if (j==0) {
					temp = arrString[j];
					arrString[j]=arrString[j+1];
					hasil +=arrString[j]+",";
				}else if(j==arrString.length-1) {
					arrString[j] = temp;
					hasil +=arrString[j];
				}else {
					arrString[j]=arrString[j+1];
					hasil +=arrString[j]+",";
				}
			}
			System.out.println((i+1)+": "+hasil);
		}
		
		
	}
	
	private static void soal3() {
		System.out.println("Di sini soal 3");
		System.out.println("Masukan jumlah Puntung : ");
		int puntung = input.nextInt();
//		int puntung = 100;
		int hargaRoko = 500;
		int batang = puntung/8;
		int penghasilan = batang*hargaRoko;
		System.out.println();
		System.out.println("Jumlah Batang Roko : "+batang);
		System.out.println("Jumlah penghasilan : "+penghasilan);
		
	}
	
	private static void soal4() {
		System.out.println("Di sini soal 4");
		
		System.out.println("Masukan jumlah menu : ");
		int jumlahMenu = input.nextInt();
		System.out.println("Makanan alergi index ke- : ");
		int indexAlr = input.nextInt();
		input.nextLine();
		System.out.println("Harga Menu : ");
		String harga = input.nextLine();
//		String harga = "12000,20000,9000,15000";
		
		String[] arrStrHarga = harga.split(",");
		int []arrIntHarga = new int[arrStrHarga.length];

		System.out.println("Uang Elsa : ");
		int uang = input.nextInt();
		//disimpang ke integer
		
		int total =0;
		for (int i = 0; i < arrStrHarga.length; i++) {
			arrIntHarga[i] = Integer.parseInt(arrStrHarga[i]);
			total = total+arrIntHarga[i];
		}
		
		total=total - arrIntHarga[indexAlr];
		int bayar = total/2;
		int sisa = uang-bayar;
		System.out.println();
		System.out.println("Elsa Harus Membayar : "+bayar);
		if (sisa>0) {
			System.out.println("Sisa Uang Elsa : "+sisa);
		} else {
			System.out.println("Uang Elsa Kurang : "+sisa);
		}
	
		
	}
	
	private static void soal5() {
		System.out.println("Di sini soal 5");
		System.out.println("Masukan teks : ");
		String teks = input.nextLine();
//		String teks = "Simple Case";
		String newTeks = teks.toLowerCase().replaceAll(" ", "");
		
		char [] arrTeks=newTeks.toCharArray();
//		System.out.println(arrTeks[2]);
		String vokal ="";
		String konsonan = "";
		
		for (int i = 0; i < arrTeks.length; i++) {
			if (arrTeks[i]=='a'||arrTeks[i]=='i'||arrTeks[i]=='u'||arrTeks[i]=='e'||arrTeks[i]=='o') {
				vokal = vokal+arrTeks[i];
			} else {
				konsonan = konsonan+arrTeks[i];
			}
		}
		
		char[] charVokal = vokal.toCharArray();
		char[] charKonsonan = konsonan.toCharArray();
		char temp;
		for (int i = 0; i < charVokal.length; i++) {
			for (int j = 0; j < charVokal.length; j++) {
				if ((int)charVokal[i]<=(int)charVokal[j]) {
					temp = charVokal[i];
					charVokal[i]=charVokal[j];
					charVokal[j]=temp;
				}
			}
			
		}
		char temp2;
		for (int i = 0; i < charKonsonan.length; i++) {
			for (int j = 0; j < charKonsonan.length; j++) {
				if ((int)charKonsonan[i]<=(int)charKonsonan[j]) {
					temp2 = charKonsonan[i];
					charKonsonan[i]=charKonsonan[j];
					charKonsonan[j]=temp2;
				}
			}
			
		}
		
		
		
		System.out.println("Vokal : "+String.valueOf(charVokal));
		System.out.println("Konsonan : "+String.valueOf(charKonsonan));
		
	}
	
	private static void soal6() {
		System.out.println("Di sini soal 6");
		System.out.println();
		System.out.println("Masukan Kalimat : ");
		String teks = input.nextLine(); //Aku sayang kamu kamu sayang aku nggak
		String[] newTeks= teks.split(" "); 
		String hasil = "";
		
		for (int i = 0; i < newTeks.length; i++) {
			char[] arrKata=newTeks[i].toCharArray(); //[aku] = [a,k,u]
			for (int j = 0; j < arrKata.length; j++) {
				if ((j+1)%2==0) {
					hasil = hasil+"*"; //hasil = a*
				}
				else {
					hasil = hasil+arrKata[j];// hasil = a*u
				}
			}
			if (i != newTeks.length-1) {
				hasil = hasil+" ";
			}
		}
		System.out.println(hasil);
	}

}
