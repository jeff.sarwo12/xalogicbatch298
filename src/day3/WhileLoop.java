package day3;

import java.util.Scanner;

public class WhileLoop {

	public static void main(String[] args) {
		int i = 0;
		Scanner input = new  Scanner(System.in);
		System.out.println("Input N : ");
		int n = input.nextInt();
		
		System.out.println();
		while (i<n) {
			System.out.println(i+1);
			i++;
		}
		
		System.out.println();
		i = 0;
		while (i<n) {
			System.out.println(n);
			n--;
		}
		

	}

}
