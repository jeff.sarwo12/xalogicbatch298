package day4;

import java.util.Scanner;

public class ArrayTwoDimension {

	public static void main(String[] args) {

		Scanner input = new  Scanner(System.in);
//		int[][] arrAngka = new int [5][5];
//		arrAngka[2][2]=13;
//		
//		System.out.println(arrAngka.length);

		System.out.println("Masukan n : ");
		int n = input.nextInt();
		
		int[][] arrAngka = new int [n][n];
		int h = 0;
		for (int i = 0; i < arrAngka.length; i++) {
			for (int j = 0; j < arrAngka.length; j++) {
				h++;
				System.out.print(h+"\t");
			}
			System.out.println();
		}
		
	}

}
