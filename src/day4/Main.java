package day4;

import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
		int[] arrayAngka = new int[5];
		
		String dua = "2";
		int tempAngka = Integer.parseInt(dua);
		
//		memasukan data ke dalam array
		arrayAngka[0] = 1;
		arrayAngka[1] = tempAngka;
		arrayAngka[2] = 3;
		arrayAngka[3] = 4;
		arrayAngka[4] = 5;
		
		//aksesAngka
		int aksesAngka = arrayAngka[1];
		
		for (int i = 0; i < arrayAngka.length; i++) {
			System.out.println(arrayAngka[i]+" ");
		}
		
		System.out.println("Panjang Array :"+arrayAngka.length);
		System.out.println(aksesAngka);
		System.out.println();
		
		String[] arrayPembelian = new String[5];
		
		int pembelianPertama = 10000;
		int pembelianKedua = 20000;
		
//		convert to String
		arrayPembelian[0] = String.valueOf(pembelianPertama);
		arrayPembelian[1] = pembelianKedua + "";
		System.out.println("Nilai Pembelian 1 : "+ arrayPembelian[0]);
		System.out.println("Nilai Pembelian 2 : "+ arrayPembelian[1]);
		
	}

}
