package day4;

import java.util.Scanner;

public class Pratic2No4 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Masukan n1 : ");
		int n1 = in.nextInt();
		System.out.println("Masukan n2 : ");
		int n2 = in.nextInt();
		int[][] arrAngka = new int[2][n1];
		int angka = 1;
		int t = n2;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n1; j++) {
				if (i == 0) {
					arrAngka[i][j] = j;
				} else {
					if ((j + 1) % 2 == 0) {
						arrAngka[i][j] = t;
						t = n2*angka;		 
					} else {
						arrAngka[i][j] = angka;

						angka = angka+1;
					}

				}
			}
		}

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n1; j++) {
				System.out.print(arrAngka[i][j] + " ");
			}
			System.out.println();
		}

	}

}
