package day4;

import java.util.Scanner;

public class pratic2No5 {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Masukan n : ");
		int n = in.nextInt();
//		int n=7;
		int[][] arrAngka = new int[3][n];
		int angka = n;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					arrAngka[i][j] = j;
				} else {
					arrAngka[i][j] = angka;
					angka = angka + 1;
				}
			}
		}

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(arrAngka[i][j] + " ");
			}
			System.out.println();
		}

	}

}
