package day9;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Scanner;

public class Main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Pilih Nomor Soal 1-8");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				default:
					System.out.println("Soal Tidak Ditemukan!");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println();
			System.out.println("Lanjut?");
			answer = input.nextLine();
		}
	}

	private static void soal1() {

		System.out.println("Ini Soal 1");

		System.out.println("Masukan Jumlah Input : ");
		int n = input.nextInt();
		BigDecimal[] arrBig = new BigDecimal[n];
		for (int i = 0; i < n; i++) {
			arrBig[i] = input.nextBigDecimal();
		}
		for (int i = 0; i < arrBig.length; i++) {
			for (int j = 0; j < arrBig.length; j++) {
				if (arrBig[i].compareTo(arrBig[j]) == -1) {
					BigDecimal temBig = arrBig[i];
					arrBig[i] = arrBig[j];
					arrBig[j] = temBig;

				}
			}
		}
		for (int i = 0; i < arrBig.length; i++) {
			System.out.println(arrBig[i]);
		}

	}

	private static void soal2() {
		System.out.println("Ini Soal 2");

		System.out.println("Masukan Jumlah Input : ");
		int n = input.nextInt();
		input.nextLine();

		System.out.println("Masukan Array : ");
		String nilai = input.nextLine();
		String[] arrString = nilai.split(" ");
		int[] arrNil = new int[n];

		if (arrString.length != n) {
			System.out.println("Panjang Inputan tidak sesuai!Ulang!");
			return;
		}

		for (int i = 0; i < n; i++) {
			arrNil[i] = Integer.parseInt(arrString[i]);
		}
		int temp = 0;
		int ind = n;
		boolean cek = false;
		System.out.println();
		for (int i = n - 1; i >= 0; i--) {
			cek = false;
			for (int j = i; j >= 0; j--) {
				if (arrNil[i] < arrNil[j]) {
					temp = arrNil[i];
					arrNil[i] = arrNil[j];
					ind = j;
					cek = true;
				}
			}
			if (cek == true) {
				for (int j = 0; j < n; j++) {
					System.out.print(arrNil[j] + " ");
				}
				System.out.println();
			}
			arrNil[ind] = temp;
		}
		for (int j = 0; j < n; j++) {
			System.out.print(arrNil[j] + " ");
		}
		System.out.println();
	}

	private static void soal3() {
		System.out.println("Ini Soal 3");
		System.out.println("Masukan Jumlah Input : ");
		int n = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Array : ");
		String nilai = input.nextLine();
		String[] arrString = nilai.split(" ");
		int[] arrNil = new int[n];
		int temp = 0;
		if (arrString.length != n) {
			System.out.println("Panjang Inputan tidak sesuai!Ulang!");
			return;
		}
		for (int i = 0; i < n; i++) {
			arrNil[i] = Integer.parseInt(arrString[i]);
		}
		for (int i = 0; i < arrNil.length; i++) {
			for (int j = 0; j < arrNil.length; j++) {
				if (arrNil[i] < arrNil[j]) {
					temp = arrNil[i];
					arrNil[i] = arrNil[j];
					arrNil[j] = temp;
				}
			}
		}
		System.out.println();
		for (int i = 0; i < arrNil.length; i++) {
			System.out.print(arrNil[i] + " ");
		}

	}

	private static void soal4() {
		System.out.println("Ini Soal 4");
		System.out.println("Masukan Jumlah Input : ");
		int n = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Array : ");
		String nilai = input.nextLine();
		String[] arrString = nilai.split(" ");
		int[] arrNil = new int[n];
		int temp = 0;
		if (arrString.length != n) {
			System.out.println("Panjang Inputan tidak sesuai!Ulang!");
			return;
		}
		for (int i = 0; i < n; i++) {
			arrNil[i] = Integer.parseInt(arrString[i]);
		}
		int jum = 0;
		for (int i = 0; i < arrNil.length; i++) {
			for (int j = 0; j < arrNil.length; j++) {
				if (arrNil[i] < arrNil[j] && j < i) {
					temp = arrNil[i];
					jum += (arrNil[j] - arrNil[i]);
					arrNil[i] = arrNil[j];
					arrNil[j] = temp;
				}
			}
		}
		System.out.println(jum);
	}
	private static void soal5() {
		System.out.println("Ini soal No 5");
		System.out.println("Masukan panjang Array : ");
		int n = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Array : ");
		String nilai = input.nextLine();
		String[] arrNilai = nilai.split(" ");
		int[] arrTemp = new int[n];
		int ind = 0;
		
		for (int i = 0; i < n; i++) {
			ind = Integer.parseInt(arrNilai[i]);
			arrTemp[ind]++;
		}
		
		for (int i = 0; i < n; i++) {
			System.out.println(arrTemp[i]+" ");
		}
	}
	private static void soal6() {
		System.out.println("Ini Soal 6 ");
		System.out.println("Masukan Jumlah Input : ");
		int n = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Array : ");
		String nilai = input.nextLine();
		String[] arrString = nilai.split(" ");
		int[] arrNil = new int[n];
		int temp = 0;
		if (arrString.length != n) {
			System.out.println("Panjang Inputan tidak sesuai!Ulang!");
			return;
		}
		for (int i = 0; i < n; i++) {
			arrNil[i] = Integer.parseInt(arrString[i]);
		}
		
		for (int i = 0; i < arrNil.length; i++) {
			for (int j = 0; j < arrNil.length; j++) {
				if (arrNil[i] < arrNil[j]) {
					temp = arrNil[i];
					arrNil[i] = arrNil[j];
					arrNil[j] = temp;
				}
			}
		}
		
		for (int i = 0; i < arrNil.length; i++) {
			System.out.print(arrNil[i] + " ");
		}
		
	}
	private static void soal7() {
		System.out.println("Ini soal No 7");
		System.out.println("Masukan Jumlah Input : ");
		int n = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Array : ");
		String nilai = input.nextLine();
		String[] arrString = nilai.split(" ");
		int[] arrNil = new int[n];
		int temp = 0;
		if (arrString.length != n) {
			System.out.println("Panjang Inputan tidak sesuai!Ulang!");
			return;
		}
		for (int i = 0; i < n; i++) {
			arrNil[i] = Integer.parseInt(arrString[i]);
		}
		for (int i = 0; i < arrNil.length; i++) {
			for (int j = 0; j < arrNil.length; j++) {
				if (arrNil[i] < arrNil[j]) {
					temp = arrNil[i];
					arrNil[i] = arrNil[j];
					arrNil[j] = temp;
				}
			}
		}
		for (int i = 0; i < arrNil.length; i++) {
			System.out.print(arrNil[i] + " ");
		}
		System.out.println();
		int median = 0;
		if (n % 2 != 0) {
			median = arrNil[n/2];
		} else {
			median = (arrNil[(n-1)/2]+arrNil[n/2])/2;
		}
		System.out.println("Nilai Median : "+median);
	}

	private static void soal8() {
		System.out.println("Ini Soal 8 ");
		System.out.println("Inputkan huruf : ");
		String huruf = input.next();
		char[] arrHuruf = huruf.toCharArray();
		char temp = ' ';
		for (int i = 0; i < arrHuruf.length; i++) {
			for (int j = 0; j < arrHuruf.length; j++) {
				if (arrHuruf[i] < arrHuruf[j]) {
					temp = arrHuruf[i];
					arrHuruf[i] = arrHuruf[j];
					arrHuruf[j] = temp;
				}
			}
		}
		System.out.println();
		for (int i = 0; i < arrHuruf.length; i++) {
			System.out.print(arrHuruf[i]);
		}

	}
}
