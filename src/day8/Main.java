package day8;

import java.util.Iterator;
import java.util.Scanner;

public class Main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Pilih Nomor Soal 1-10 : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case id not available");
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal1() {
		System.out.println("Carmel Case");
		System.out.println("Inputkan String : ");
		String teks = input.nextLine();
		char[] arrTeks = teks.toCharArray();

		int jum = 1;
		for (int i = 0; i < arrTeks.length; i++) {
			if (Character.isUpperCase(arrTeks[i])) {
				jum++;
			}

		}

		System.out.println(jum);

	}

	private static void soal2() {
		System.out.println("Strong Password");
		System.out.print("Inputkan jumlah karakter : ");
		int n = input.nextInt();
		input.nextLine();
		System.out.println("Inputkan Password : ");
		String pass = input.nextLine();
		char[] arrPass = pass.toCharArray();

		if (n != arrPass.length) {
			System.out.println("Jmlah Karakter Tidak Sesuai ! ");
			return;
		}

		String spesialCek = "!@#$%^&*()-+";
		int spesial = 0;
		int numbers = 0;
		int lowerCase = 0;
		int upperCase = 0;
		int salah = 0;
		if (arrPass.length >= 6) {
			for (int i = 0; i < arrPass.length; i++) {
				if (Character.isUpperCase(arrPass[i])) {
					upperCase++;
				}
				if (Character.isLowerCase(arrPass[i])) {
					lowerCase++;
				}
				if (Character.isDigit(arrPass[i])) {
					numbers++;
				}
				if (spesialCek.contains(Character.toString(arrPass[i]))) {
					spesial++;
				}
			}
			if (upperCase == 0) {
				salah += 1;
			}
			if (lowerCase == 0) {
				salah += 1;
			}
			if (numbers == 0) {
				salah += 1;
			}
			if (spesial == 0) {
				salah += 1;
			}
		} else {
			salah = 6 - arrPass.length;
		}
		System.out.println(salah);
	}

	private static void soal3() {
		System.out.println("Caesar Cipher\n");
		char[] oriAlphabet = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
				's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
		char[] rotAlphabet = oriAlphabet.clone();
		System.out.print("Inputkan jumlah karakter : ");
		int n = input.nextInt();
		input.nextLine();
		System.out.println("Inputkan teks : ");
		String teks = input.nextLine();
		char[] arrTeks = teks.toCharArray();
		String rotTeks = "";

		if (n != arrTeks.length) {
			System.out.println("Jumlah Karakter Tidak Sesuai !");
		}

		System.out.print("Inputkan Jumalah Rotasi : ");
		int rot = input.nextInt();
		char temp = ' ';
		for (int i = 0; i < rot; i++) {
			for (int j = 0; j < rotAlphabet.length; j++) {
				if (j == 0) {
					temp = rotAlphabet[j];
					rotAlphabet[j] = rotAlphabet[j + 1];
				} else if (j == rotAlphabet.length - 1) {
					rotAlphabet[j] = temp;
				} else {
					rotAlphabet[j] = rotAlphabet[j + 1];
				}
			}
		}

		for (int i = 0; i < arrTeks.length; i++) {
			boolean alpha = false;
			boolean upper = false;
			if (Character.isUpperCase(arrTeks[i])) {
				upper = true;
				arrTeks[i] = Character.toLowerCase(arrTeks[i]);
			}
			for (int j = 0; j < oriAlphabet.length; j++) {
				if (arrTeks[i] == oriAlphabet[j]) {
					if (upper == true) {
						rotTeks += Character.toUpperCase(rotAlphabet[j]);
					} else {
						rotTeks += rotAlphabet[j];
					}
					alpha = true;
				}
			}
			if (alpha == false) {
				rotTeks += arrTeks[i];
			}
		}
		System.out.println("Hasil Enkripsi : " + rotTeks);
	}

	private static void soal4() {
		System.out.println(" ");
		System.out.println("Gak Dikerjain Gess");

	}

	private static void soal5() {
		System.out.println("HackerRank in a String!\n");
		System.out.println("Inputkan Jumlah Kata Yang Ingin Dicek : ");
		int n = input.nextInt();
		int ulang = 0;
		char[] cekHek = { 'h', 'a', 'c', 'k', 'e', 'r', 'r', 'a', 'n', 'k' };
		boolean[] arrCek = new boolean[n];
		input.nextLine();
		while (ulang<n) {
			System.out.println("Masukan Teks ke-"+ulang+" : ");
			String teks = input.nextLine();
			char[] arrChar = teks.toLowerCase().toCharArray();
			int val = 10;
			int benar = 0;
			int bat = 0;

			for (int i = 0; i < cekHek.length; i++) {// i = 0
				boolean get = false;
				for (int j = bat; j < arrChar.length; j++) { // j = 0,
					if (cekHek[i] == arrChar[j] && get == false) {
						benar++;
						bat = j;
						get = true;
					}
				}
			}
			if (val == benar) {
				arrCek[ulang] = true; 
			} else {
				arrCek[ulang] = false;
			}
			ulang++;
		}
		System.out.println();
		for (int i = 0; i < arrCek.length; i++) {
			if (arrCek[i] == true) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}
		}
	}

	private static void soal6() {
		System.out.println("Simple Array Sum\n");
		char[] arrAlpa = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
				's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
		System.out.println("Inputkan Kalimat : ");
		String kalimat = input.nextLine();
		char[] arrKalimat = kalimat.toLowerCase().toCharArray();

		for (int i = 0; i < arrAlpa.length; i++) {
			boolean val = false;
			for (int j = 0; j < arrKalimat.length; j++) {
				if (arrAlpa[i] == arrKalimat[j]) {
					val = true;
				}
				if (arrKalimat[i] == 0) {
					val = true;
				}
			}
			if (val == false) {
				System.out.println("not pangram");
				return;
			}
		}
		System.out.println("pangram");
	}

	private static void soal7() {
		System.out.println("Separate the NumbersPage");
		System.out.println("Masukan Jumlah Perulangan : ");
		int n = input.nextInt();
		input.nextLine();
		boolean[] arrVal = new boolean[n];
		int[] arrAwal = new int[n];
		for (int i = 0; i < n; i++) {
			System.out.println("Masukan Nilai ke-"+(i+1)+" : ");
			String Num = input.nextLine();
			String[] arrNum = Num.split("");
			
			int mulai = 0;
			int sub =1;
			int tam =0;
			int tam2 =0;
			int has = 0;
			boolean val = false;
			boolean run = true;
			
			
			while (run == true) {
				
				tam = Integer.parseInt(Num.substring(0, sub)) ;
				has = tam;
				int pan= Num.substring(0, sub).length();
				mulai = pan;
				boolean jalan = true;
				int tengah = arrNum.length/2;
				if (sub>tengah || Integer.parseInt(Num.substring(0, sub))==0 ) {
					jalan = false;
					run = false;
					val = false;
				}
				
				while (jalan == true) {
					tam2 =Integer.parseInt(Num.substring(mulai, (mulai+pan)))  ;
					int tar = tam2*10;
					int end = mulai+pan;
					if (tam2<tam) {
						sub=sub+1;
						jalan = false;
						val = false;
						if (tar-1==tam) {
							pan++;
							jalan = true;
						}
					}
					if (tam2==tam) {
						sub=sub+1;
						val = false;
						jalan = false;
					}
					if (tam == tam2-1) {
						tam = tam2;
						mulai = (mulai-1)+pan;
						jalan = true;
						val = true;
						mulai++;
					}
					if (end == arrNum.length) {
						jalan = false;
						run = false;
					}
				}
			}
			arrVal[i] = val;
			arrAwal[i] = has;
		}

		for (int i = 0; i < arrVal.length; i++) {
			if(arrVal[i]==true) {
				System.out.println("Yes "+arrAwal[i]);
			}else {
				System.out.println("No ");
			}
		}
		
	}

	private static void soal8() {
		System.out.println("Masukan jumlah inputan : ");
		int n = input.nextInt();
		input.nextLine();
		if (n<=1) {
			System.out.println("Jumlah Inputan Harus Lebih dari 1!");
			return;
		}
		System.out.println("Input ke-1 : ");
		String in1 = input.nextLine();
		char[] arrIn1 = in1.toLowerCase().toCharArray();
		char[] arrPen = new char[arrIn1.length];
		
		int ulang = 0;
		while (ulang<n-1) {
			System.out.println("Input ke-"+(ulang+2)+" : ");
			String in2 = input.nextLine();
			char[] arrIn2 = in2.toLowerCase().toCharArray();
			
			for (int i = 0; i < arrIn1.length; i++) {
				boolean val = false;
				for (int j = 0; j < arrIn2.length; j++) {
					if (arrIn1[i]==arrIn2[j]) {
						arrPen[i]=arrIn1[i];
						arrIn2[j]='*';
						val = true;
					}
				}
				if (val==false) {
					arrPen[i]='*';
				}
			}
			arrIn1=arrPen.clone();
			ulang++;
		}
		int jum = 0;
		for (int i = 0; i < arrPen.length; i++) {
			if (arrPen[i]!='*') {
				jum++;
			}
		}
		System.out.println();
		System.out.println(jum);
	}

	private static void soal9() {
		System.out.println("Making Anagram\n");
		System.out.println("Inputkan 1 : ");
		String ang1 = input.nextLine();
		System.out.println("Inputkan 2 : ");
		String ang2 = input.nextLine();
		
		char[] arrAnag1 = ang1.toCharArray();
		char[] arrAnag2 = ang2.toCharArray();
		char[] tampAnag = new char[arrAnag1.length];
		
		if (arrAnag1.length != arrAnag2.length) {
			System.out.println("Jumlah karakter Tidak Sama! Ulang!");
			return;
		}

		

		for (int i = 0; i < arrAnag1.length; i++) {
			for (int j = 0; j < arrAnag2.length; j++) {
				if (arrAnag1[i] == arrAnag2[j]) {
					tampAnag[j] = arrAnag2[j];
					arrAnag2[j] = '*';
				}
			}
		}
		int del = 0;
		for (int i = 0; i < tampAnag.length; i++) {
			if (tampAnag[i] == 0) {
				del++;
			}
		}
		System.out.println(del * 2);
	}

	private static void soal10() {
		System.out.println("Two String\n");
		System.out.println("Inputkan jumlah pengecekan : ");
		int n = input.nextInt();
		boolean[] arrVal = new boolean[n];
		input.nextLine();
		int ulang = 0;
		while (ulang < n) {
			System.out.println();
			System.out.println("Pengecekan ke-" + (ulang + 1));
			System.out.println("Kata a : ");
			String kat1 = input.nextLine();
			System.out.println("Kata b : ");
			String kat2 = input.nextLine();
			
			char[] arrKet1 = kat1.toLowerCase().toCharArray();
			char[] arrKet2 = kat2.toLowerCase().toCharArray();
			boolean val = false;
			for (int i = 0; i < arrKet1.length; i++) {
				for (int j = 0; j < arrKet2.length; j++) {
					if (arrKet1[i] == arrKet2[j]) {
						val = true;
					}
				}
			}
			arrVal[ulang] = val;
			ulang++;
		}
		System.out.println();
		for (int i = 0; i < arrVal.length; i++) {
			if (arrVal[i] == true) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}
		}
	}
	
	

}
