package prates;

import java.text.ParseException;
import java.util.Iterator;
import java.util.Scanner;

public class Main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Pilih Nomor Soal 1-10");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Soal Tidak Ditemukan!");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println();
			System.out.println("Lanjut?");
			answer = input.nextLine();
		}

	}

	private static void soal1() {
		System.out.println("Input n : ");
		int n = input.nextInt();

		for (int i = 1; i <= n; i++) {
			if (i % 2 != 0) {
				System.out.print(i + " ");
			}
		}

		System.out.println();
		for (int i = 1; i <= n; i++) {
			if (i % 2 == 0) {
				System.out.print(i + " ");
			}
		}
	}

	private static void soal2() {
		System.out.println("Input teks : ");
		String n = input.nextLine();
		n = n.toLowerCase().replace(" ", "");
//		System.out.println(n);
		char[] arrN = n.toCharArray();
		for (int i = 0; i < arrN.length; i++) {
			for (int j = 0; j < arrN.length; j++) {
				if (arrN[i] < arrN[j]) {
					char temp = arrN[i];
					arrN[i] = arrN[j];
					arrN[j] = temp;
				}
			}
		}
		String vokal = "";
		String konsonan = "";
		for (int i = 0; i < arrN.length; i++) {
			if (arrN[i] == 'a' || arrN[i] == 'i' || arrN[i] == 'u' || arrN[i] == 'e' || arrN[i] == 'o') {
				vokal = vokal + arrN[i];
			} else {
				konsonan = konsonan + arrN[i];
			}

		}
		System.out.println("Huruf Vokal : " + vokal);
		System.out.println("Huruf Konsonan : " + konsonan);

	}

	private static void soal3() {
		System.out.println("Input n : ");
		int n = input.nextInt();
		int nilai = 100;
		int tmbah = 3;
		for (int i = 0; i < n; i++) {
			System.out.println(nilai + " adalah Si Angka 1 ");
			nilai += tmbah;
			tmbah += 3;
		}
		input.nextLine();
	}

	private static void soal4() {
		System.out.println("0. Toko");
		System.out.println("1. Tempat 1");
		System.out.println("2. tempat 2");
		System.out.println("4. tempat 4");
		System.out.println("3. tempat 3");
		System.out.println("Pilih titik awal sampai titik akhir (gunakan ',' untuk memisahkan ) : ");
//		System.out.println("Input n : ");
		String sRute = input.nextLine();
		String[] arrRute = sRute.split(",");
		int[] ruteSoal = new int[arrRute.length];
		for (int i = 0; i < arrRute.length; i++) {
			ruteSoal[i] = Integer.parseInt(arrRute[i]);
		}
		double[] rute = { 0, 2, 0.5, 1.5, 2.5 };
		double tempuh = 0;
		int rs = 0;
		double jarakBensin = 2.5;
		for (int i = 0; i < ruteSoal.length; i++) {
			if (ruteSoal[i] < rs) {
				for (int j = 0; j <= ruteSoal[i - 1]; j++) {
					tempuh += rute[j];
				}
			} else if (rs != ruteSoal[i]) {
				tempuh += rute[ruteSoal[i]];
			}
			rs = ruteSoal[i];

		}
		jarakBensin = Math.ceil(tempuh / jarakBensin);
		int bensin = (int) jarakBensin;
		System.out.println("Total jarak tempuh : " + tempuh + " KM");
		System.out.println("Total Bensin yang digunakan : " + bensin + " Liter");

//		if () {
//
//		}
	}

	private static void soal5() {
//		System.out.println("Input n : ");
		double lakiDewasa = 4;
		double peremDewasa = 1;
		double remaja = 0;
		double balita = 1;
		boolean ulang = true;
		while (ulang == true) {
			System.out.println("1. Laki-laki : " + (int) lakiDewasa);
			System.out.println("2. Perempuan : " + (int) peremDewasa);
			System.out.println("3. Remaja : " + (int) remaja);
			System.out.println("4. Balita : " + (int) balita);
			System.out.println("Pilih yang ingin tambahkan (0 untuk tidak ) : ");
			int in = input.nextInt();
			if (in == 0) {
				ulang = false;
			} else {
				System.out.println("Jumlah yang ingin ditambahkan  : ");
				int tambah = input.nextInt();

				if (in == 1) {
					lakiDewasa += tambah;
				}
				if (in == 2) {
					peremDewasa += tambah;
				}
				if (in == 3) {
					remaja += tambah;
				}
				if (in == 4) {
					balita += tambah;
				}

			}

		}

		double total = (lakiDewasa * 2) + (peremDewasa * 1) + (remaja * 2) + (balita * 0.5);
		total = Math.ceil(total);

		if (total % 2 != 0 && total > 5) {
			total += peremDewasa;
		}
		System.out.println("Total Porsi : " + (int) total);
		input.nextLine();

	}

	private static void soal6() {
		String pin = "123456";

		System.out.print("Masukan Pin : ");
		String in = input.next();
		int uang = 0;
		int pil = 0;
		int tf = 0;
		int admin = 7500;
		String rekTujuan = "";
		String kodeBank = "";
		boolean transfer = false;

		if (in.equals(pin)) {
			System.out.println();
			System.out.println("Uang yang disetor : ");
			uang = input.nextInt();

			System.out.println("Pilih jenis transfer : 1. Antar Rekening 2. Antar Bank");
			System.out.print("\nPilih : ");
			pil = input.nextInt();
		} else {
			System.out.println("Maaf Pin Anda Salah ");
			return;
		}
		if (pil == 1) {
			System.out.print("\nMasukan rekening tujuan : ");
			rekTujuan = input.next();
			System.out.print("\nMasukan Nominal Transfer : ");
			tf = input.nextInt();
			transfer = true;
		} else if (pil == 2) {
			System.out.print("\nMasukan Kode Bank : ");
			kodeBank = input.next();
			System.out.print("\nMasukan rekening tujuan : ");
			rekTujuan = input.next();
			System.out.print("\nMasukan Nominal Transfer : ");
			tf = input.nextInt();
			tf = tf + admin;
			if (kodeBank.length() != 3) {
				System.out.println("Kode Bank Salah ! ulang!");
				return;
			}
			transfer = true;
		}

		if (transfer == true) {
			if (rekTujuan.length() != 10) {
				System.out.println("Rekening Salah Ulang !");
				return;
			}
			if (tf > uang) {
				System.out.println("Saldo Tidak Mencukupi !");
				return;
			} else {
				uang = uang - tf;
				System.out.println("Transaksi Berhasil, Saldo Anda saat ini Rp. " + uang);
			}
		}
	}

	private static void soal7() {
		System.out.println("Masukan Jumlah Kartu : ");
		int kartu = input.nextInt();
		String main = "Y";

		while (main.toUpperCase().equals("Y")) {
			System.out.println("Taruhan : ");
			int taruhan = input.nextInt();
			input.nextLine();
			System.out.println("Tebak Kotak (A/B) : ");
			String tebak = input.nextLine();
			int angkaA = (int) (Math.random() * 10);
			int angkaB = (int) (Math.random() * 10);

			if (taruhan > kartu) {
				System.out.println("Maaf Taruhan Anda Melebihi kartu !");
			} else if (angkaA > angkaB && tebak.toUpperCase().equals("A")
					|| angkaA < angkaB && tebak.toUpperCase().equals("B")) {
				System.out.println("Kotak A : " + angkaA);
				System.out.println("Kotak B : " + angkaB);
				System.out.println("You Win!");
				kartu = kartu + taruhan;
				System.out.println("kartu saat ini : " + kartu);

			} else if (angkaA == angkaB) {
				System.out.println("Seri !");
			} else {
				System.out.println("Kotak A : " + angkaA);
				System.out.println("Kotak B : " + angkaB);
				System.out.println("You Lose! ");
				kartu = kartu - taruhan;
				System.out.println("kartu saat ini : " + kartu);
			}

			if (kartu <= 0) {
				System.out.println("kartu Anda Telah Habis!");
				main = "N";
			} else {
				System.out.println();
				System.out.println("Main Lagi? (Y/N) : ");
				main = input.nextLine();
			}

		}

		System.out.println("Permainan Telah Berakhir");
	}

	private static void soal8() {
		System.out.println("Input N : ");
		int n = input.nextInt();

		input.nextLine();
		int genap = 0;
		int ganjil = 1;
		String tamp = "";
		for (int i = 0; i < n; i++) {
			int hasil = genap + ganjil;

			if (i == n - 1) {
				tamp = tamp + hasil;
			} else {
				tamp = tamp + hasil + ",";
			}
			genap += 2;
			ganjil += 2;
		}
		System.out.println(tamp);
		return;
	}

	private static void soal9() {
		System.out.println("Input : ");
		String teks = input.nextLine();
		char[] arrTeks = teks.toLowerCase().replace(" ", "").toCharArray();
		int n = 0;
		int l = 0;
		int lembah = 0;
		int gunung = 0;
		for (int i = 0; i < arrTeks.length; i++) {
			if (arrTeks[i] == 'n') {
				n += 1;
				l -= 1;
				if (l == 0) {
					lembah += 1;
				}
			} else {
				n -= 1;
				l += 1;
				if (n == 0) {
					gunung += 1;
				}
			}

		}
		System.out.println("Jumlah Gunung : " + gunung);
		System.out.println("Jumlah Lembah : " + lembah);

	}

	private static void soal10() {
		int cup = 18000;
//		int saldo = 60000;
		System.out.println("Masukan Saldo : ");
		int saldo = input.nextInt();
		int total = 0;
		double totalAkhir = 0;
		double dis = 0;
		double back = 0;
		int totalCup = 0;
		boolean jalan = true;
		while (jalan == true) {
			total += cup;
			totalCup++;
			dis = total;
			back = (0.5 * total) * 0.1;
			if (total >= 40000) {
				dis = total - (0.5 * total);
			}
			if (dis > 100000) {
				dis = 100000;
			}
			if (back > 30000) {
				back = 30000;
			}
			totalAkhir = dis - back;

			if (totalAkhir + cup > saldo) {
				jalan = false;
			}
		}

		System.out.println("Cash Back : " + back);
		System.out.println("Diskon : " + dis);
		System.out.println("Total : " + total);
		System.out.println("Setelah Diskon + CB : " + totalAkhir);
		System.out.println("Total Cup : " + totalCup);
	}

}
