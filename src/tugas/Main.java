package tugas;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;
//import java.util.*;

public class Main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) throws Exception {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Pilih Nomor Soal 1-8");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				default:
					System.out.println("Soal Tidak Ditemukan!");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println();
			System.out.println("Lanjut?");
			answer = input.nextLine();
		}

	}

	private static void soal1() {
		System.out.println("Ini Soal 1");

		System.out.println("Masukan Jumlah Point : ");
		int point = input.nextInt();
		String main = "Y";

		while (main.toUpperCase().equals("Y")) {
			System.out.println("Taruhan : ");
			int taruhan = input.nextInt();
			input.nextLine();
			System.out.println("Tebak(U/D) : ");
			String tebak = input.nextLine();
			int angka = (int) (Math.random() * 10);

			if (taruhan > point) {
				System.out.println("Maaf Taruhan Anda Melebihi Point !");
			} else if (angka > 5 && tebak.toUpperCase().equals("U") || angka < 5 && tebak.toUpperCase().equals("D")) {
				System.out.println("Hasil : " + angka);
				System.out.println("You Win!");
				point = point + taruhan;
				System.out.println("Point saat ini : " + point);

			} else if (angka == 5) {
				System.out.println("Seri !");
			} else {
				System.out.println("Hasil : " + angka);
				System.out.println("You Lose! ");
				point = point - taruhan;
				System.out.println("Point saat ini : " + point);
			}

			if (point <= 0) {
				System.out.println("Point Anda Telah Habis!");
				main = "N";
			} else {
				System.out.println();
				System.out.println("Main Lagi? (Y/N) : ");
				main = input.nextLine();
			}

		}

		System.out.println("Permainan Telah Berakhir");
	}

	private static void soal2() {
		System.out.println("Soal 2 ");
		String[] krnjng = new String[3];
		int total = 0;
		for (int i = 0; i < krnjng.length; i++) {
			System.out.println("Keranjang " + (i + 1) + " : ");
			krnjng[i] = input.nextLine();
			if (krnjng[i].toLowerCase().equals("kosong")) {
				krnjng[i] = "0";
			}
		}
		System.out.println("Pilih keranjang yang ingin dibawah ke pasar (1-3) : ");
		String pilih = input.nextLine();
		String[] arrPilih = pilih.split(",");
		for (int i = 0; i < arrPilih.length; i++) {
			krnjng[Integer.parseInt(arrPilih[i]) - 1] = "0";
		}
//		krnjng[pilih-1] = "0";
//		krnjng[pilih2-1] = "0";
		total = Integer.parseInt(krnjng[0]) + Integer.parseInt(krnjng[1]) + Integer.parseInt(krnjng[2]);
		System.out.println("Sisa buah : " + total);

//		System.out.print("Pilih Keranjang Kosong (1-3) : ");
//		int ksng = input.nextInt();
//		if (ksng <= 0 || ksng > 3) {
//			System.out.println("Keranjang tidak ada ! ");
//			return;
//		}
//		int[] arrKrg = new int[3];
//		arrKrg[ksng - 1] = 0;
//		for (int i = 0; i < arrKrg.length; i++) {
//			if ((i + 1) != ksng) {
//				System.out.print("Jumlah buah di keranjang " + (i + 1) + " : ");
//				arrKrg[i] = input.nextInt();
//			}
//		}
//
//		System.out.println("Pilih Keranjang yang ingin dibawa ke pasar : ");
//		int p = input.nextInt();
//		input.nextLine();
//		arrKrg[p - 1] = 0;
//		int sisa = arrKrg[0] + arrKrg[1] + arrKrg[2];
//		System.out.println();
//		System.out.println("Keranjang " + p + " dibawa ke pasar\n");
//
//		System.out.println("Sisa Buah : " + sisa);

	}

	private static void soal3() {
		System.out.println("Soal 3 ");
		System.out.print("Masukan Jumlah Anak : ");
		int x = input.nextInt();
		input.nextLine();
		int cara = 1;
		// !4 = 4*3*2*1 =
		for (int i = 1; i <= x; i++) {
			cara = cara * i;
		}
		System.out.println("ada " + cara + " cara");
	}

	private static void soal4() {

		int lakiDewasa = 5;
		int wanitaDewasa = 3;
		int anak = 3;
		int bayi = 1;

		int totalBaju = lakiDewasa + (wanitaDewasa * 2) + (anak * 3) + (bayi * 5);

		if (totalBaju % 2 != 0 && totalBaju > 10) {
			totalBaju += wanitaDewasa;
		}

		System.out.println("Total Baju : " + totalBaju);
	}

	private static void soal5() {
		System.out.println("Soal No 5 ");
//			System.out.println("Masukan jumlah Pukis : ");
		int pukis = 15;
		float terigu = 125f;
		float gulaPasir = 100f;
		float susuMurni = 100f;
		float putihTelur = 100f;
		DecimalFormat des = new DecimalFormat("0.00");
		System.out.println("Berikut Jumlah Bahan yang diperlukan untuk membuat 1 kue pukis : ");
		System.out.println("-tepung terigu 	  : " + (des.format(terigu / pukis)) + " gram");
		System.out.println("-gula pasir		  : " + (des.format(gulaPasir / pukis)) + " gram");
		System.out.println("-susu murni 	  : " + (des.format(susuMurni / pukis)) + " gram");
		System.out.println("-putih telur ayam : " + (des.format(putihTelur / pukis)) + " gram");
	}

	private static void soal6() throws Exception {
		System.out.println("Input tgl dan waktu masuk gedung (dd/MM/yyyy HH.mm) : ");
		String tglMasuk = input.nextLine();
		Date date1 = new SimpleDateFormat("dd/MM/yyyy HH.mm").parse(tglMasuk);
		System.out.println("Input tgl dan waktu keluar gedung (dd/MM/yyyy HH.mm) : ");
		String tglKeluar = input.nextLine();
		Date date2 = new SimpleDateFormat("dd/MM/yyyy HH.mm").parse(tglKeluar);
		long parkir = 0;
		long selisih = Math.abs(date2.getTime() - date1.getTime());
		System.out.println(date2.getTime());
		long selisihJam = selisih / (60 * 60 * 1000);
		long selisihMenit = selisih / (60 * 1000) % 60;
		long selisihHari = selisih / (24 * 60 * 60 * 1000);

		if (selisihMenit > 0) {
			selisihJam += 1;
		}
		parkir = selisihHari > 0 ? 15000 * selisihHari : 3000 * selisihJam;
		System.out.println();
//		System.out.println("Selisih Hari  : " + selisihHari + " Hari");
		System.out.println("Durasi Parkir : " + selisihJam + " Jam");
		System.out.println("Biaya Parkir : Rp." + parkir);

	}

	private static void soal7() throws Exception {
		System.out.println("Input tanggal ambil (dd-MM-yyyy) :");
		String tglMasuk = input.nextLine();
		Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(tglMasuk);
		System.out.println("Input tanggal kembali (dd-MM-yyyy) :");
		String tglKeluar = input.nextLine();
		Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(tglKeluar);
		long selisih = Math.abs(date2.getTime() - date1.getTime());
		long selisihHari = selisih / (24 * 60 * 60 * 1000);
		long telat = 0;

		if (selisihHari > 3) {
			telat = selisihHari - 3;
		}
		int denda = (int) telat * 500;
		System.out.println("Telat mengembalikan : " + telat + " Hari");
		System.out.println("Denda yang harus dibayarkan : Rp." + denda);
	}

	private static void soal8() {
		System.out.println("Soal No 8 ");
		System.out.println("Masukan teks : ");
		String teks = input.nextLine().toLowerCase();
		String teks2 = "";
		int j = 0;
		for (int i = 1; i <= teks.length(); i++) {
			teks2 = teks2 + teks.substring(teks.length() - i, teks.length() - j);
			j++;
		}
		if (teks.toLowerCase().equals("bakso")||teks.toLowerCase().equals("sop")) {
			System.out.println("TUMPAH");
		}
		else if (teks2.equals(teks)) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}
	}
}
