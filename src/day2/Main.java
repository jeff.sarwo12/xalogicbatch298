package day2;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in); //in = input
		System.out.println("Input Hadiah (Pilih Nomor 1-5)");
		
		int nomor= in.nextInt();
		
		switch (nomor) {
		case 1:
			System.out.println("Selamat Anda Mendapatkan Mobil");
			break;
		case 2:
			System.out.println("Selamat Anda Mendapatkan Tiket Pulang ke Bandung");
			break;
		case 3:
			System.out.println("Selamat Anda Mendapatkan Boneka Beruang");
			break;
		case 4:
			System.out.println("Selamat Anda Mendapatkan Liburan Ke India");
			break;
		case 5:
			System.out.println("Selamat Anda Mendapatkan Motor Harley Jawa");
			break;
		default:
			System.out.println("Nomor yang anda pilih tidak sesuai!");
			break;
		}
		
		
	}
}
