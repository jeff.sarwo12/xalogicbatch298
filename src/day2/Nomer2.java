package day2;

import java.util.Scanner;

public class Nomer2 {
	public static void main (String[] args) {
		Scanner in = new Scanner(System.in);
		
		int ongkir = 5000;
		int diskon = 0;
		String kodePromo = "JKTOVO";
		
		System.out.println("Belanja : ");
		int belanja = in.nextInt();
		System.out.println("Jarak : ");
		int jarak = in.nextInt();
		
		in.nextLine();
		System.out.println("Masukan Kode Promo : ");
		String inputKode = in.nextLine();
		
		if (jarak>5) {
			ongkir = jarak*1000;
		} 

		if (inputKode.equals(kodePromo)) {
			if (belanja >= 30000) {
				diskon = (int) (0.4 * belanja);
			}
		}
		else {
			diskon = 0;
		}
		
		if (diskon>30000) {
			diskon = 30000;
		}

		int total = (int) (belanja - diskon);
		total = total+ongkir;
		System.out.println("-------------");
		System.out.println("Belanja : "+belanja);
		System.out.println("Diskon 40% : "+diskon);
		System.out.println("Ongkir : "+ongkir);
		System.out.println("Total Belanja : "+total);

	}
}
