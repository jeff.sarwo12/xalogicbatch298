package day2;

import java.util.Scanner;

public class Nomer3 {
	public static void main (String[] args) {
		Scanner in = new Scanner(System.in);
		
		int disBelanja;
		int disOngkir;
		
		System.out.println("Belanja : ");
		int belanja = in.nextInt();
		System.out.println("Ongkos Kirim : ");
		int ongkir = in.nextInt();
		
		System.out.println("--------------------------");
		
		if (belanja>=100000) {
			disBelanja = 20000;
			disOngkir = 10000;
		}else if(belanja>=50000) {
			disBelanja = 10000;
			disOngkir = 10000;
		}else if(belanja>=30000) {
			disBelanja = 5000;
			disOngkir = 5000;
		}else {
			disBelanja = 0;
			disOngkir = 0;
		}
		
		int total = (belanja-disBelanja)+(ongkir-disOngkir);
		
		System.out.println("Belanja : "+belanja);
		System.out.println("Ongkos Kirim : "+ongkir);
		System.out.println("Diskon Ongkir : "+disOngkir);
		System.out.println("Diskon Belanja : "+disBelanja);
		System.out.println("Total Belanja : "+total);
	}
	
}
