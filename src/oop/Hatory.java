package oop;

public class Hatory {
	private int jmlh = 0;
	private int sisaEn = 0;
	
	public void lari(int energi) {
		sisaEn = energi;
		jmlh = 0;
		while (sisaEn>=6) {
			jmlh++;
			sisaEn-=6;
		}
	}
	public void jalan_cepat(int energi) {
		sisaEn = energi;
		jmlh = 0;
		while (sisaEn>=4) {
			jmlh++;
			sisaEn-=4;
		}
	}
	
	public void jalan_biasa(int energi) {
		sisaEn = energi;
		jmlh = 0;
		while (sisaEn>=2) {
			jmlh++;
			sisaEn-=2;
		}
	}
	
	public int getSisa() {
		return sisaEn;
	}
	public int getJumlah() {
		return jmlh;
	}
	
}
