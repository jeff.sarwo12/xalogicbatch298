package day6;

import java.util.Scanner;

public class Main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				default:
					System.out.println("Case id not available");
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal1() {
		System.out.println();
		System.out.println("Di sini soal 1");
		System.out.println("Masukan n : ");
		int n = input.nextInt();
		String tampil = "";
		int fib1 = 1;
		int fib2 = 1;
		int temp = 0;
		for (int i = 0; i < n; i++) {
			if (i == 0) {
				tampil = tampil + fib1 + ","; // tampil = 1,
			} else {
				temp = fib1; // temp = 1,2,3
				fib1 = fib1 + fib2;// angka = 2,3,5
				fib2 = temp; // angka 2 = 1,2,3
				if (i != n - 1) {
					tampil = tampil + temp + ","; // tampil = 1,2,
				} else {
					tampil = tampil + temp; // tampil = 1,2,
				}

			}

		}

		System.out.println(tampil);
	}

	private static void soal2() {
		System.out.println("Di sini soal 2");

		System.out.println("Input n : ");
		int n = input.nextInt();

		int fib1 = 1;
		int fib2 = 1;
		int fib3 = 1;
		int temp = 0;
		String tampil = "";
		for (int i = 0; i < n; i++) {
			if (i < 2) {
				tampil = tampil + 1 + ",";// 1,1,1
			} else {
				temp = fib1; // 1,3,5,
				fib1 = fib1 + fib2 + fib3; // 3,5,9
				fib3 = fib2;// 1,1,
				fib2 = temp;// 1,3,
				if (i != n - 1) {
					tampil = tampil + temp + ",";// 1,1,1
				} else {
					tampil = tampil + temp;
				}
			}
		}
		System.out.println(tampil);

	}

	private static void soal3() {
		System.out.println("Di sini soal 3");
		System.out.println("Input n : ");
		int n = input.nextInt();
		String tamp = "";
		int bil = 0;
		for (int i=0; i<n; i++)
        {
            bil=0;
            for (int j=1; j<=i; j++)
            {
                if (i%j==0)
                {
                    bil = bil+1;
                }
            }
            if (bil==2)
            {
            	tamp = tamp+i+",";
//                System.out.print(i+" ");
            }
        }
		
		System.out.println(tamp);

	}

	private static void soal4() {
		System.out.println("Di sini soal 4");
		System.out.println("Inputkan Waktu : ");
		String wktu12 = input.nextLine().toUpperCase();
		String[] arrWktu12 = wktu12.substring(0, 8).split(":");
		String val = wktu12.substring(8, 10);
		String tampil = "";
		
		if (val.equals("PM")&&Integer.parseInt(arrWktu12[0])<12) {
			tampil = (Integer.parseInt(arrWktu12[0]) + 12) + "." + arrWktu12[1] + "." + arrWktu12[2];
		} else if(val.equals("AM")&&Integer.parseInt(arrWktu12[0])==12){
			arrWktu12[0] = "00";
			tampil = arrWktu12[0] + "." + arrWktu12[1] + "." + arrWktu12[2];
		}
		else {
			tampil = arrWktu12[0] + "." + arrWktu12[1] + "." + arrWktu12[2];
		}
		System.out.println(tampil);
	}

	private static void soal5() {
		System.out.println("Di sini soal 5");
		System.out.println("Masukan niali n : ");
		int n = input.nextInt();
		int prima = 2; //awal prima
		int result = 0;
		while (n != 1) { // kondisi n belim satu
			result = n; //sebagai pembanding
			if (result%prima==0) { //prima = 2,
				result = n/prima;
				System.out.println(""+n+"/"+prima+"="+result);
				n = result; //n diganti menjadi hasil pembagian
			} else {
				prima++; //prima tambah satu karna belum memenuhi kondisi
			}
		}
//		int fak = 0;
//		int bag = n/2;
//		String tampPrim = "";
//		int bil = 0;
//		for (int i=0; i<=bag; i++)
//        {
//            bil=0;
//            for (int j=1; j<=i; j++)
//            {
//                if (i%j==0)
//                {
//                    bil = bil+1;
//                }
//            }
//            if (bil==2)
//            {
//            	tampPrim = tampPrim+i+" ";
//            }
//        }
//		
//		String[] arrFak = tampPrim.split(" ");
//		int i = 0;
//		while (i < arrFak.length) {
//			if (n % Integer.parseInt(arrFak[i]) == 0) {
//				fak = n / Integer.parseInt(arrFak[i]);
//				System.out.println(n + "/" + arrFak[i] + "= " + fak);
//				n = n / Integer.parseInt(arrFak[i]);
//				i = 0;
//			}else {
//				i++;
//			}
//		}

	}

	private static void soal6() {
		System.out.println("Di sini soal 6");
		System.out.println("Input titik perjalanan : ");
		String titik = input.nextLine();
		String[] arrTitikS = titik.split(" ");
		double jarak = 0;
		String tamp = "";
		String ket = "";
		double jt = 0;
//		System.out.println(arrTitikS.length);
		for (int i = 0; i < arrTitikS.length; i++) {
			int cv=Integer.parseInt(arrTitikS[i]);
			if (cv==1) {
				jt = 2;
				ket = "2KM";
			} else if(cv==2){
				jt = 0.5;
				ket = "500M";
			}else if(cv==3) {
				jt = 1.5;
				ket = "1.5KM";
			}else {
				jt = 0.3;
				ket = "300M";
			}
			
			if (i!=arrTitikS.length-1) {
				jarak = jarak+jt;
				tamp = tamp+ket+"+";
			} else {
				jarak = jarak+jt;
				tamp = tamp+ket+"="+jarak+"KM";
			}
		}
		double lit=jarak/2.5;
		int bensin = 0;
		for (int i = 0; i < lit; i++) {
			bensin +=1;
		}
		System.out.println();
		System.out.println("Jarak Tempuh : "+tamp);
		System.out.println("Bensin : "+bensin+" Liter");

	}

	private static void soal7() {
		System.out.println("Di sini soal 7");
		
		System.out.println("Masukan sinyal : ");
		String stringSos = input.nextLine();
		//menyesuakan jika sinya tidak sos
		if (stringSos.length()%3!=0) {
			stringSos=stringSos+"*";
			if (stringSos.length()%3!=0) {
				stringSos=stringSos+"*";
			}
		}
		char[] arrSinyal = stringSos.toLowerCase().toCharArray();
		int hit = arrSinyal.length/3;
		String sos = "";
		for (int i = 0; i < hit; i++) {
			sos+="sos";
		}
		char[] arrSOS = sos.toCharArray();
		int salah = 0;
		String temSOS = "";
		for (int i = 0; i < arrSOS.length; i++) {
			if (arrSOS[i]!=arrSinyal[i]) {
				salah += 1;
			}
			temSOS +=arrSOS[i];
		}
		System.out.println("Sinyal yang benar : "+temSOS.toUpperCase());
		System.out.println("Sinya yang diterima signal : "+stringSos.toUpperCase());
		System.out.println("Total Sinyal Salah : "+salah);
		

	}
}
